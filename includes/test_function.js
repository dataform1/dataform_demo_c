function test_loop(n) {
  var indices = [];
  for (var i = 1; i <= n; i++) {
    indices.push(i);
  }
  return `${indices.join(", ")}`;
}

module.exports = { test_loop };